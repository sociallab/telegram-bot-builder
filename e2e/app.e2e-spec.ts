import { TablighgramPage } from './app.po';

describe('tablighgram App', () => {
  let page: TablighgramPage;

  beforeEach(() => {
    page = new TablighgramPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
