export class TelegramBot {
    _id: string;
    title: string;
    token: string;
    username: string;
    enabled: string;
    task_id: string;
    expire: string;
}