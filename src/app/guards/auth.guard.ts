﻿import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { JwtHelper } from './../helper/jwtHelper'
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate() {

        const jwtHelper = new JwtHelper();
        let token = localStorage.getItem('currentUser');
        
        if (token != null && !jwtHelper.isTokenExpired(token)) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }
}