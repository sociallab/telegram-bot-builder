import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TelegramBotComponent } from './components/telegramBots/telegramBots.component';
import { TelegramBotService } from './services/telegramBot.service'
import { HttpModule } from '@angular/http';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard'
import { AuthenticationService } from './services/authentication.service'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TelegramBotsFormComponent } from './components/telegramBots/telegramBots-form.component';
import { TelegramBotsDetailComponent } from './components/telegramBots/telegramBots-detail.component';
// import { TreeModule } from 'ng2-tree';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { TreeModule, FileUploadModule } from 'primeng/primeng';
import { ModalModule } from 'ng2-bootstrap/modal';
import { TelegramBotsUsersComponent } from './components/telegramBots/telegramBots-users.component';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { MaterialModule } from '@angular/material';
import { AppLoginComponent } from './components/appLogins/appLogin.component';
import { SelectModule } from 'ng2-select';
import { AppLoginService } from './services/appLogin.service'


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    TreeModule,
    ModalModule.forRoot(),
    SimpleNotificationsModule,
    FileUploadModule,
    SlimLoadingBarModule.forRoot(),
    MaterialModule.forRoot(),
    SelectModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    TelegramBotComponent,
    LoginComponent,
    TelegramBotsFormComponent,
    TelegramBotsDetailComponent,
    TelegramBotsUsersComponent,
    AppLoginComponent
  ],
  providers: [
    TelegramBotService,
    AuthGuard,
    AuthenticationService,
    AppLoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }