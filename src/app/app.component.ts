import { Component, OnDestroy } from '@angular/core';
import '../../public/css/style.css';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'تبلیغگرام';

  private sub: any;
  // Position of Ng2ToastyComponent
  private toastyComponentPosition: string;

  constructor(private slimLoader: SlimLoadingBarService, private router: Router, ) {
    // Listen the navigation events to start or complete the slim bar loading


    this.sub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        // this.slimLoader.color = 'blue';
        this.slimLoader.height = '5px';
        this.slimLoader.start();
      } else if (event instanceof NavigationEnd ||
        event instanceof NavigationCancel ||
        event instanceof NavigationError) {
        this.slimLoader.complete();
      }
    }, (error: any) => {
      this.slimLoader.complete();
    });

  }
  ngOnDestroy(): any {
    this.sub.unsubscribe();
  }
}