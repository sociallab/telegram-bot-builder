import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TelegramBotComponent } from './components/telegramBots/telegramBots.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard'
import { TelegramBotsFormComponent } from './components/telegramBots/telegramBots-form.component';
import { TelegramBotsDetailComponent } from './components/telegramBots/telegramBots-detail.component';
import { TelegramBotsUsersComponent } from './components/telegramBots/telegramBots-users.component';
import { AppLoginComponent } from './components/appLogins/appLogin.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]

  },
  {
    path: 'telegrambots',
    component: TelegramBotComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'addtelegrambots',
    component: TelegramBotsFormComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'telegrambots/:id',
    component: TelegramBotsDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'telegrambotusers/:id',
    component: TelegramBotsUsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'applogins',
    component: AppLoginComponent,
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true });
