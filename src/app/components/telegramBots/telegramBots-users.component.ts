import { Component, OnInit, OnDestroy } from '@angular/core'
import { NotificationsService } from 'angular2-notifications';
import { TelegramBot } from "./../../models/telegramBot";
import { TelegramBotService } from './../../services/telegramBot.service'
import { Router, ActivatedRoute } from '@angular/router';
// import { TreeModule, TreeNode, FileUploadModule, Message } from 'primeng/primeng';
import { AuthenticationService } from './../../services/authentication.service';
import { DefaultResult } from './../../models/defaultResult'


// import $ = require('jquery')
declare var $: JQueryStatic;


import './telegramBots-users.component.css';

@Component({
    // selector: 'p-tree',
    templateUrl: './telegramBots-users.component.html',
    styleUrls: ['./telegramBots-users.component.css']
})
export class TelegramBotsUsersComponent implements OnInit, OnDestroy {
    constructor(private _notyService: NotificationsService,
        private _telegramBotService: TelegramBotService,
        private _route: ActivatedRoute,
        private authenticationService: AuthenticationService) {
    }

    public ngOnInit() {
        this.getOpTypes()

        this.sub = this._route.params
            .subscribe(params => {
                this.botId = params['id'];

            });
    }
    ngOnDestroy(): void {
        this.sub.unsubscribe();

    }
    public options = {
        timeOut: 5000,
        rtl: true
    }
    private sub: any;
    private botId: string;
    SendList: Array<any> = []
    SendTypes: Array<any> = [
        {
            id: 0,
            title: 'همه'
        },
        {
            id: 1,
            title: 'شخص'
        }
    ];
    SendType: number = 0;
    TelegramUserId = '';
    opModel = <any>{
        selectedoperationType: 1,
        content:
        {
            files: []
        }

    }
    hightlightStatus: Array<boolean> = [];
    operationTypes = <any>[]
    public resethightlightStatus(e: number) {
        for (let i = 0; i < this.hightlightStatus.length; i++) {
            this.hightlightStatus[i] = false;
        }
        this.hightlightStatus[e] = true;
    }
    selectedOperationId: number;
    public getOpTypes() {
        this._telegramBotService.getBotOperations()
            .subscribe(
            optypes => {
                this.operationTypes = optypes;
            },
            error => this._notyService.error('error', error));
    }
    public send() {
        this._telegramBotService.send(this.botId, this.SendList)
            .subscribe((resp: DefaultResult) => {
                if (resp.success) {
                    this._notyService.success('success', resp.msg)
                } else {
                    this._notyService.error('error', resp.msg)
                }

                // this._router.navigate(["/telegrambots"]);
            }, (err) => {
                this._notyService.error('error', err)
            });
    }
    isOpSelected: boolean;
    public opSelect(i: number) {
        this.selectedOperationId = i;
        this.isOpSelected = true;
        this.opModel.selectedoperationType = this.SendList[i].type;
        // this.opModel.title = this.SendList[i].title;
        this.opModel.content = this.SendList[i].content;
        // this.opModel.title = this.operationTypes[this.opModel.selectedoperationType].title
    }

    public addOperation() {
        // e.preventDefault();
        // if (this.SendList.length > 0) {
        this.opModel = {
            selectedoperationType: 1,
            title: 'none',
            content:
            {
                files: []
            }
        }
        // }
        // this.opModel.title = this.operationTypes[this.opModel.selectedoperationType].title
        this.SendList.push({
            title: `عملیات ${this.SendList.length}`,
            type: 1,
            content: this.opModel.content,
        });
        this.opModel = {
            selectedoperationType: 1,
            content:
            {
                files: []
            }
        }

        this.opSelect(this.SendList.length - 1)
        this.resethightlightStatus(this.SendList.length - 1)
        // if (this.SendList.length === 1) {
        //     this.addOperation()
        // }
    }
    public editOperation() {
        // this.SendList[this.selectedOperationId].title = this.opModel.title;
        this.SendList[this.selectedOperationId].type = this.opModel.selectedoperationType;
        this.SendList[this.selectedOperationId].content = this.opModel.content;
    }
    public deleteOperation() {
        var index = this.SendList.indexOf(this.SendList[this.selectedOperationId]);
        if (index > -1) {
            this.SendList.splice(index, 1);
        }
    }
    onChange(deviceValue: any) {
        this.SendList[this.selectedOperationId].type = this.opModel.selectedoperationType;
        this.SendList[this.selectedOperationId].title = this.operationTypes[this.opModel.selectedoperationType].title;

        console.log(deviceValue);
    }
    uploadedFiles: any[] = [];

    onUpload(event: any) {
        // for (let file of event.files) {
        //     this.uploadedFiles.push(file);

        // }
        // this.opModel.files = [];

        // console.log(this.opModel)

        if (this.opModel.content.text) {
            this.opModel.content.text = ''
        }
        if (!this.opModel.content.files) {
            this.opModel.content.files = []
        }
        for (let item of JSON.parse(event.xhr.response)) {
            this.opModel.content.files.push({
                type: item.mimetype,
                filename: item.filename,
                originalname: item.originalname,
                size: item.size,
            })
        }

        console.log(event)
        this._notyService.info('info', 'File Uploaded');
    }
    onUpError(e: any) {

        try {
            this._notyService.error('error', JSON.parse(e.xhr.response).message);
        } catch (error) {
            this._notyService.error('error', 'error in uploading');

        }

        console.log(e)
    }
    onBeforeUpSend(event: any) {
        // this._notyService.error('error', 'error in uploading');
        event.xhr.setRequestHeader("Authorization", this.authenticationService.token);

        console.log(event)
    }
    deleteFile(i: number) {
        this._telegramBotService.deleteFile(this.botId, this.SendList[this.selectedOperationId].content.files[i].filename)
            .subscribe(
            res => {
                if (res.success) {
                    console.log(res)
                    var index =
                        this.SendList[this.selectedOperationId].content.files.indexOf(this.SendList[this.selectedOperationId].content.files[i]);
                    if (index > -1) {
                        this.SendList[this.selectedOperationId].content.files.splice(index, 1);
                    }
                    this._notyService.success('Success', 'فایل با موفقیت حذف شد');
                } else {
                    if (res.msg == "No file with that identifier has been found") {
                        var index =
                            this.SendList[this.selectedOperationId].content.files.indexOf(this.SendList[this.selectedOperationId].content.files[i]);
                        if (index > -1) {
                            this.SendList[this.selectedOperationId].content.files.splice(index, 1);
                        }
                        this._notyService.success('Success', 'فایل با موفقیت حذف شد');
                    } else {
                        this._notyService.error('Error', 'در حذف کردن فایل مشکلی پیش آمده است');
                        console.log('_notyService.error')
                    }


                }
            },
            error => {
                this._notyService.error('Error', error);
            });


    }

}