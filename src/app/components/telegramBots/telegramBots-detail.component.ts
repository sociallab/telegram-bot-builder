import { Component, OnInit, OnDestroy } from '@angular/core'
import { NotificationsService } from 'angular2-notifications';
import { TelegramBot } from "./../../models/telegramBot";
import { TelegramBotService } from './../../services/telegramBot.service'
import { Router, ActivatedRoute } from '@angular/router';
import { TreeModule, TreeNode, FileUploadModule, Message } from 'primeng/primeng';
import { AuthenticationService } from './../../services/authentication.service';


// import $ = require('jquery')
declare var $: JQueryStatic;
interface Data {
    id?: string,
    parentId?: string;
    parent?: TreeNode;
    enabled?: boolean;
    operations?: any,
    column?: number
}

interface ServerPattern {
    data: Array<TreeNode>
}


// declare var JSONPath: any;

import './telegramBots-detail.component.css';

@Component({
    // selector: 'p-tree',
    templateUrl: './telegramBots-detail.component.html',
    styleUrls: ['./telegramBots-detail.component.css']
})
export class TelegramBotsDetailComponent implements OnInit, OnDestroy {
    constructor(private _notyService: NotificationsService,
        private _telegramBotService: TelegramBotService,
        private _route: ActivatedRoute,
        private authenticationService: AuthenticationService) {
        // 
        //  this.ref.detectChanges();
        // setInterval(() => {
        //     this.ref.detectChanges();
        // }, 1000);
    }


    hightlightStatus: Array<boolean> = [];

    operations = Array<any>();
    nodes: TreeNode[];
    public selectedNode: TreeNode;
    public selectedId: string;
    public anySelected: boolean;

    public getOpTypes() {
        this._telegramBotService.getBotOperations()
            .subscribe(
            optypes => {
                this.operationTypes = optypes;
            },
            error => this._notyService.error('error', error));
    }
    public getTree() {
        var data_main: Data = {
            enabled: true,
            id: 'main',
            operations: [],
            parentId: '-',
            column: 3
        };

        this._telegramBotService.getBotMenu(this.botId)
            .subscribe(
            tree => {
                let exp = (tree.length > 0) ? true : false
                this.nodes = [{
                    label: 'منو اصلی',
                    children: tree,
                    data: data_main,
                    expanded: exp
                }];
            },
            error => {
                this.errorMessage = <any>error
                this._notyService.error('error', 'خطایی در دریافت منوی ربات رخ داده است')
            });


    }

    public ngOnInit() {
        this.getOpTypes()
        this.sub = this._route.params
            .subscribe(params => {
                this.botId = params['id'];

            });


        // this.getTree();
        // this._telegramBotService.getFiles().then(nodes => this.nodes = nodes);

        // this._telegramBotService.getFiles().then(nodes => {
        //     // this.nodes = nodes
        //     this.nodes = [{
        //         label: 'منو اصلی',
        //         children: nodes,
        //         expanded: true
        //     }];
        // });

        this.getTree();

        $(document).ready(function () {
            $('#title').emojiPicker({
                width: '300px',
                height: '200px'
            });

        });



    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
    public options = {
        timeOut: 5000,
        rtl: true
    }
    botId: string;
    private sub: any;
    loading = false;
    error: any;
    errorMessage: string;
    model = <any>{
        checked: true,
        column: 3
    }
    opModel = <any>{
        selectedoperationType: 0,
        content:
        {
            files: []
        }

    }
    // selectedOperation: any;

    operationTypes = <any>[]

    listOne: Array<any> = [
        {
            title: "ارسال متن",
            type: 1,
            content: { text: 'درود' }
        },
        {
            title: "ارسال عکس",
            type: 2,
            content: { text: 'url' }
        },
        {
            title: "ارسال فیلم",
            type: 1,
            content: { text: 'درود' }
        }
    ];

    public onNodeSelect(e: any) {
        console.log(e);
        this.anySelected = true;
        this.selectedNode = e.node

        this.listOne = this.selectedNode.data.operations;

        this.model.title = e.node.label;
        this.selectedId = e.node.data.id;
        this.model.enabled = e.node.data.enabled;
        this.model.parent = e.node.data.parentId;
        // this._notyService.info('onNodeSelect', 'selected')
        // this.opSelect(0)
        if (this.selectedNode.data.operations.length > 0) {
            this.opSelect(this.selectedNode.data.operations.length - 1)
            this.resethightlightStatus(this.selectedNode.data.operations.length - 1)
        }
    }



    public add() {

        if (this.anySelected) {
            var data_ading: Data = {
                enabled: this.model.enabled,
                id: this.makeid(),
                operations: [],
                parentId: this.selectedNode.data.id,
                column: this.model.column
                // parent: this.selectedNode
            };
            this.selectedNode.expandedIcon = 'fa-folder-open';
            this.selectedNode.collapsedIcon = 'fa-folder';
            let title = $('#title').val();
            this.selectedNode.children.push({
                label: title,
                children: [],
                // parent: this.selectedNode,
                data: data_ading,
            });
            this.listOne = this.selectedNode.data.operations;
            this.selectedNode.expanded = true;
            console.log(this.nodes)
        } else {
            this._notyService.alert('انتخاب شاخه والد', 'لطفا اول یک شاخه والد برای درج انتخاب کنید')
        }

    }
    public edit() {

        if (this.anySelected) {
            let title = $('#title').val();
            this.selectedNode.label = title;
        } else {
            this._notyService.alert('انتخاب شاخه', 'شاخه مورد نظر را انتخاب کنید.')
        }
    }

    public delete() {
        if (this.anySelected) {
            // this.selectedNode
            if (this.selectedNode.data.id !== 'main') {
                this.find(this.nodes[0], this.selectedNode.data.parentId);
                let parent: TreeNode = this.foundValue;
                this.found = false;
                console.log(parent)

                if (parent.data.id !== this.selectedNode.data.parentId)
                    console.log('Wrrrrrrrrrrrrrrrrrooooooooooooooonnnnnnnnnnnnnnnggggggggggg')

                var index = parent.children.indexOf(this.selectedNode);
                if (index > -1) {
                    parent.children.splice(index, 1);
                } else {
                    console.log('-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1')

                }
            }

        } else {
            this._notyService.alert('انتخاب شاخه', 'شاخه مورد نظر را انتخاب کنید.')
        }
        this.anySelected = false;
        this.selectedNode = null;
    }




    // public handleMoved(e: NodeEvent): void {
    //     console.log(e);
    // }

    // public handleCreated(e: NodeEvent): void {
    //     console.log(e);
    // }

    public foundValue: TreeNode;
    public found: boolean;

    public find(tree: TreeNode, id: string) {



        if (tree.data.id === id) {
            console.log(tree);
            console.log(tree.data.id);
            this.found = true;
            this.foundValue = tree;
            return;
        } else {
            for (var i in tree.children) {
                // avoid circular reference infinite loop & skip inherited properties
                if (tree.children.length === 0) continue;

                this.find(tree.children[i], id);
                if (this.found === true) return;
            }
        }
    }

    public delteC(tree: TreeNode) {
        if (tree.data.parent) {
            tree.data.parent = '';
        }
        if (tree.children.length > 0) {
            for (var i in tree.children) {
                // avoid circular reference infinite loop & skip inherited properties
                if (tree.children.length === 0) continue;
                this.delteC(tree.children[i]);
            }
        }
    }


    private makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 30; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }


    public resethightlightStatus(e: number) {
        for (let i = 0; i < this.hightlightStatus.length; i++) {
            this.hightlightStatus[i] = false;
        }
        this.hightlightStatus[e] = true;
    }
    selectedOperationId: number;
    isOpSelected: boolean;
    public opSelect(i: number) {
        this.isOpSelected = true;
        this.selectedOperationId = i;
        if (this.selectedNode.data.operations.length > 0) {
            this.opModel.selectedoperationType = this.selectedNode.data.operations[i].type;
            this.opModel.title = this.selectedNode.data.operations[i].title;
            this.opModel.content = this.selectedNode.data.operations[i].content;
        } else {
            this.opModel.selectedoperationType = 0
        }

    }
    newOperation: any = []

    public addOperation(e: any) {
        // e.preventDefault();
        this.opModel = {
            selectedoperationType: 1,
            title: 'none',
            content:
            {
                files: []
            }
        }
        this.selectedNode.data.operations.push({
            title: 'ارسال متن',
            type: 1,
            content: this.opModel.content,
        });
        this.listOne = this.selectedNode.data.operations;

        this.opModel = {
            selectedoperationType: 1,
            content:
            {
                files: []
            }
        }

        this.opSelect(this.selectedNode.data.operations.length - 1)
        this.resethightlightStatus(this.selectedNode.data.operations.length - 1)
    }
    onChange(deviceValue: any) {
        this.selectedNode.data.operations[this.selectedOperationId].type = this.opModel.selectedoperationType;
        this.selectedNode.data.operations[this.selectedOperationId].title = this.operationTypes[this.opModel.selectedoperationType].title;
        this.opModel.title = this.operationTypes[this.opModel.selectedoperationType].title;
        // console.log(deviceValue);
    }
    public editOperation() {
        this.selectedNode.data.operations[this.selectedOperationId].title = this.opModel.title;
        this.selectedNode.data.operations[this.selectedOperationId].type = this.opModel.selectedoperationType;
        this.selectedNode.data.operations[this.selectedOperationId].content = this.opModel.content;
        this.listOne = this.selectedNode.data.operations;
    }
    public deleteOperation() {
        var index = this.selectedNode.data.operations.indexOf(this.selectedNode.data.operations[this.selectedOperationId]);
        if (index > -1) {
            this.selectedNode.data.operations.splice(index, 1);
        }
    }


    onSubmit(event: any): void {
        event.preventDefault();
        // this.delteC(this.nodes[0]);
        console.log(this.model);
        let
            sp: ServerPattern = {
                data: this.nodes[0].children
            }
        this._telegramBotService.putBotMenu(sp, this.botId)
            .subscribe((resp: any) => {
                this.loading = false;
                this._notyService.success('success', 'تعییرات با موفقیت ذخیره شد.')
                // this._router.navigate(["/telegrambots"]);
            }, (err) => {
                this.loading = false;
                this.error = err;
                this._notyService.error('error', err)
            });
    }
    goBack() {
        // window.history.back();

        console.log(this.nodes);



    }

    uploadedFiles: any[] = [];

    onUpload(event: any) {
        // for (let file of event.files) {
        //     this.uploadedFiles.push(file);

        // }
        // this.opModel.files = [];

        // console.log(this.opModel)

        if (this.opModel.content.text) {
            this.opModel.content.text = ''
        }
        if (!this.opModel.content.files) {
            this.opModel.content.files = []
        }
        for (let item of JSON.parse(event.xhr.response)) {
            this.opModel.content.files.push({
                type: item.mimetype,
                filename: item.filename,
                originalname: item.originalname,
                size: item.size,
            })
        }

        console.log(event)
        this._notyService.info('info', 'File Uploaded');
    }
    onUpError(e: any) {

        try {
            this._notyService.error('error', JSON.parse(e.xhr.response).message);
        } catch (error) {
            this._notyService.error('error', 'error in uploading');

        }

        console.log(e)
    }
    onBeforeUpSend(event: any) {
        // this._notyService.error('error', 'error in uploading');
        event.xhr.setRequestHeader("Authorization", this.authenticationService.token);

        console.log(event)
    }
    deleteFile(i: number) {
        this._telegramBotService.deleteFile(this.botId, this.selectedNode.data.operations[this.selectedOperationId].content.files[i].filename)
            .subscribe(
            res => {
                if (res.success) {
                    console.log(res)
                    var index =
                        this.selectedNode.data.operations[this.selectedOperationId].content.files.indexOf(this.selectedNode.data.operations[this.selectedOperationId].content.files[i]);
                    if (index > -1) {
                        this.selectedNode.data.operations[this.selectedOperationId].content.files.splice(index, 1);
                    }
                    this._notyService.success('Success', 'فایل با موفقیت حذف شد');
                } else {
                    if (res.msg == "No file with that identifier has been found") {
                        var index =
                            this.selectedNode.data.operations[this.selectedOperationId].content.files.indexOf(this.selectedNode.data.operations[this.selectedOperationId].content.files[i]);
                        if (index > -1) {
                            this.selectedNode.data.operations[this.selectedOperationId].content.files.splice(index, 1);
                        }
                        this._notyService.success('Success', 'فایل با موفقیت حذف شد');
                    } else {
                        this._notyService.error('Error', 'در حذف کردن فایل مشکلی پیش آمده است');
                        console.log('_notyService.error')
                    }


                }
            },
            error => this.errorMessage = <any>error);
    }

    addBack() {
        if (this.anySelected) {
            this.find(this.nodes[0], this.selectedNode.data.parentId);

            let parent: TreeNode = this.foundValue;
            this.found = false;


            this.selectedNode.data.operations.push({
                title: 'برگشت',
                type: 2,
                content: { redirect: parent.data.parentId },
            });

            this.listOne = this.selectedNode.data.operations;

            this.opModel = {
                selectedoperationType: 2,
                content:
                {
                    files: []
                }
            }

            this.opSelect(this.selectedNode.data.operations.length - 1)
            this.resethightlightStatus(this.selectedNode.data.operations.length - 1)
        }
    }
    addBackMenu() {
        if (this.anySelected) {

            this.selectedNode.data.operations.push({
                title: 'برگشت به منو',
                type: 2,
                content: { redirect: 'main' },
            });
            this.listOne = this.selectedNode.data.operations;

            this.opModel = {
                selectedoperationType: 2,
                content:
                {
                    files: []
                }
            }

            this.opSelect(this.selectedNode.data.operations.length - 1)
            this.resethightlightStatus(this.selectedNode.data.operations.length - 1)
        }
    }
}