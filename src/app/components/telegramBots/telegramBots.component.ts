import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { TelegramBot } from '../../models/telegramBot'
import { DefaultResult } from '../../models/defaultResult'
import { TelegramBotService } from './../../services/telegramBot.service'
import { NotificationsService } from 'angular2-notifications';


@Component({
    selector: 'my-bots',
    templateUrl: './telegramBots.components.html'
})
export class TelegramBotComponent implements OnInit {

    public options = {
        timeOut: 5000,
        rtl: true
    }
    telegramBots: TelegramBot[];
    selectedTelegramBot: TelegramBot;
    error: any;
    errorMessage: string;
    constructor(private _notyService: NotificationsService,
        private _router: Router,
        private telegramBotService: TelegramBotService) { }

    getBots() {
        this.telegramBotService.getTelegramBot()
            .subscribe(
            telegramBots => this.telegramBots = telegramBots,
            error => this.errorMessage = <any>error);
    }
    ngOnInit() {
        this.getBots();
    }

    startTask(index: number) {
        console.log(index);
        this.telegramBotService.setTaskState(this.telegramBots[index]._id, this.telegramBots[index].task_id, true)
            .subscribe(
            res => {
                if (res.success) {
                    this._notyService.success('Success', 'ربات با موفقیت اجرا شد');
                } else {
                    this._notyService.error('Error', 'در اجرای ربات مشکلی پیش آمده است');
                }
            },
            error => this.errorMessage = <any>error);
    }
    stopTask(index: number) {
        console.log(index);
        this.telegramBotService.setTaskState(this.telegramBots[index]._id, this.telegramBots[index].task_id, false)
            .subscribe(
            res => {
                if (res.success) {
                    console.log('_notyService.success')
                    this._notyService.success('Success', 'ربات با موفقیت متوقف شد');
                } else {
                    this._notyService.error('Error', 'در متوقف کردن ربات مشکلی پیش آمده است');
                    console.log('_notyService.error')

                }
            },
            error => this.errorMessage = <any>error);
    }
    gotTotUsers(index: number) {
        
        this._router.navigate(['/telegrambotusers', this.telegramBots[index]._id])
    }
}