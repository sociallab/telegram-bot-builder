import { Component } from "@angular/core";
import { Router } from "@angular/router";
import './telegramBots-form.component.css';

import { TelegramBot } from "./../../models/telegramBot";
import { TelegramBotService } from "./../../services/telegramBot.service";

@Component({
    //selector: 'product-form',
    templateUrl: "./telegramBots-form.component.html",
    styleUrls: ['./telegramBots-form.component.css']
})
export class TelegramBotsFormComponent {
    loading = false;
    error = ''
    model = <TelegramBot>{}; // creates an empty object of an interface

    constructor(private _telegramBotService: TelegramBotService, private _router: Router) { }



    onSubmit(): void {
        console.log(this.model);

        this._telegramBotService.addTelegramBot(this.model)
            .subscribe((product: TelegramBot) => {
                this.loading = false;
                this._router.navigate(["/telegrambots"]);
            }, (err) => {
                this.loading = false;
                this.error = err;
            });
    }
    goBack() {
        window.history.back();
    }
}