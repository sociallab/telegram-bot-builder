import { Component, OnChanges, EventEmitter, Input, Output } from '@angular/core'
import { Router } from '@angular/router'
import './appLogin.component.css'
import { DefaultResult } from './../../models/defaultResult'
import { NotificationsService } from 'angular2-notifications';
import { AppLoginService } from './../../services/appLogin.service'

@Component({
    selector: 'app-login',
    templateUrl: 'appLogin.component.html',
    styleUrls: ['./appLogin.component.css']
})
export class AppLoginComponent {

    constructor(private _appLoginService: AppLoginService,
        private _notyService: NotificationsService) { }


    public options = {
        timeOut: 5000,
        rtl: true
    }
    @Input() selectedApp: string;
    model: any = {};
    loading = false;
    // Apps = ['اینستاگرام','تلگرام'
    // ];
    Apps = [
        { id: 'instagram', text: 'اینستاگرام' },
        { id: 'telegram', text: 'تلگرام' },
    ];
    active = [
        { id: 'instagram', text: 'اینستاگرام' },
    ];

    AppLogin() {
        this._appLoginService.instagramLogin(this.model.username, this.model.password, 'instagram')
            .subscribe((resp: DefaultResult) => {
                if (resp.success) {
                    this._notyService.success('success', resp.msg)
                } else {
                    this._notyService.error('error', resp.msg)
                }

                // this._router.navigate(["/telegrambots"]);
            }, (err) => {
                this._notyService.error('error', err)
            });
    }

}