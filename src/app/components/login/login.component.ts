﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import './login.component.css';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }

    login(form: any): void {
        console.log(form);
        console.log("login fired");
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(result => {
                console.log(result)
                console.log('result')
                if (result === true) {
                    this.router.navigate(['/']);
                } else {
                    this.error = 'نام کاربری یا کلمه عبور اشتباه است';
                    this.loading = false;
                }
            }, (err) => {
                    this.loading = false;

                if (err === 'Unauthorized') {
                    this.error = 'نام کاربری یا کلمه عبور اشتباه است';
                } else {
                    this.error = err;

                }
            });
    }
}
