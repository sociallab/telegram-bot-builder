import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { AuthenticationService } from './authentication.service';
import { DefaultResult } from './../models/defaultResult'

@Injectable()
export class AppLoginService {


      constructor(private _http: Http,
        private authenticationService: AuthenticationService) { }

        private _appLoginUrl = '/api/applogin';

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
    instagramLogin(username: string, password: string, app: string) {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token }); // for ASP.NET MVC
        let options = new RequestOptions({ headers: headers });
        let body = {
            username: username,
            password: password,
            app: app,
        }
        return this._http.post(`${this._appLoginUrl}/${app}/`, JSON.stringify(body), options)
            .map((response: Response) => <DefaultResult>response.json())
            .do(data => console.log("response: " + JSON.stringify(data)))
            .catch(this.handleError);
    }

}