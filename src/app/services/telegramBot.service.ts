import { Injectable } from '@angular/core';
import { TelegramBot } from './../models/telegramBot'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { TreeNode } from 'primeng/primeng';
import { DefaultResult } from './../models/defaultResult'


@Injectable()
export class TelegramBotService {
    private _telegramBotUrl = '/api/bot';

    constructor(private _http: Http,
        private authenticationService: AuthenticationService) { }

    getTelegramBot(): Observable<TelegramBot[]> {

        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this._http.get(this._telegramBotUrl, options)
            .map((response: Response) => <TelegramBot[]>response.json())
            .do(data => console.log("All: " + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    addTelegramBot(telegramBot: TelegramBot): Observable<TelegramBot> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token }); // for ASP.NET MVC
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._telegramBotUrl, JSON.stringify(telegramBot), options)
            .map((response: Response) => <TelegramBot>response.json())
            .do(data => console.log("telegramBot: " + JSON.stringify(data)))
            .catch(this.handleError);
    }
    getBotOperations() {
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this._http.get(`${this._telegramBotUrl}/getoperations`, options)
            .map((response: Response) => <any>response.json())
            .do(data => console.log("All: " + JSON.stringify(data)))
            .catch(this.handleError);
    }

    putBotMenu(menu: any, botId: string): Observable<TelegramBot> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token }); // for ASP.NET MVC
        let options = new RequestOptions({ headers: headers });

        return this._http.put(`${this._telegramBotUrl}/menu/${botId}`, JSON.stringify(menu), options)
            .map((response: Response) => <any>response.json())
            .do(data => console.log("bootMenuResp: " + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getBotMenu(botId: string) {
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this._http.get(`${this._telegramBotUrl}/menu/${botId}`, options)
            .map((response: Response) => <TreeNode[]>response.json().data)
            .do(data => console.log("All: " + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getFiles() {
        return this._http.get('/public/data/files.json')
            .toPromise()
            .then(res => <TreeNode[]>res.json().data)
            .then(data => { return data; });
    }

    setTaskState(botId: string, taskId: string, state: boolean): Observable<DefaultResult> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token }); // for ASP.NET MVC
        let options = new RequestOptions({ headers: headers });
        let body = {
            state: state,
            taskId: taskId
        }
        return this._http.post(`${this._telegramBotUrl}/task/${botId}`, JSON.stringify(body), options)
            .map((response: Response) => <DefaultResult>response.json())
            .do(data => console.log("telegramBot: " + JSON.stringify(data)))
            .catch(this.handleError);
    }
    deleteFile(botId: string, filename: string): Observable<DefaultResult> {
        console.log(filename)
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token }); // for ASP.NET MVC
        // let options = new RequestOptions({ headers: headers });
        let body = {
            filename: filename,
        }

        return this._http.delete(`${this._telegramBotUrl}/file/${botId}`, new RequestOptions({
            headers: headers,
            body: body
        }))
            .map((response: Response) => <DefaultResult>response.json())
            .do(data => console.log("telegramBot: " + JSON.stringify(data)))
            .catch(this.handleError);
    }
    send(botId: string, sendFile: Object, ): Observable<DefaultResult> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token }); // for ASP.NET MVC
        let options = new RequestOptions({ headers: headers });
        let body = {
            sendList: sendFile,
        }
        return this._http.post(`${this._telegramBotUrl}/send/${botId}`, JSON.stringify(body), options)
            .map((response: Response) => <DefaultResult>response.json())
            .do(data => console.log("telegramBot: " + JSON.stringify(data)))
            .catch(this.handleError);
    }
    //stopTask

}