var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
ObjectId = mongoose.Types.ObjectId;

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function (property) {
    return ((this.provider !== 'local' && !this.updated) || property.length);
};

var telegramUserBotSchema = new Schema({
    telegramUserId: {
        type: String,
        trim: true,
        default: '',
        index: true,
        validate: [validateLocalStrategyProperty, 'لطفا یوزر Id را وارد کنید']
    },
    bot_id: {
        type: Schema.Types.ObjectId,
        default: null,
        ref: 'bot',
        index: true
    },
    state: {
        type: String,
        trim: true,
        default: 'main',
    },
    lastLogError: {
        type: String,
        trim: true,
        default: '',
    },
});

telegramUserBotSchema.statics.UserExistsInBot = function (telegramUserId, botId, callback) {
    var _this = this;

    _this.find({
        $and: [{ bot_id: ObjectId(botId) }, { telegramUserId: telegramUserId }]
    }, function (err, items) {
        if (!err) {
            if (items.length) {
                callback(null, items[0])
            } else {
                callback(null, false)
            }
        } else {
            callback(err)
        }
    })
}
mongoose.model('telegramUserBot', telegramUserBotSchema);