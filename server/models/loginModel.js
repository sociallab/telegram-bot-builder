var mongoose = require('mongoose');

var Schema = mongoose.Schema


let loginSchema = new Schema({
    cookie: {
        type: String,
        default: '',
    },
    path: {
        type: String,
        default: '',
    },
    app: {
        type: String,
        default: '',
    },
    token: {
        type: String,
        default: '',
    },
    username: {
        type: String,
        default: '',
        required: true
    },
    code: {
        type: String,
        default: '',
    },
    user_id: {
        type: Schema.Types.ObjectId,
        default: null,
        index: true,
        ref: 'user'
    }
})
mongoose.model('login', loginSchema);