var mongoose = require('mongoose');

var Schema = mongoose.Schema


let autoFollowTaskSchema = new Schema({
    content: {
        type: String,
    },
    cursor: {
        type: String,
        index: true
    },
    expired: {
        type: Boolean,
        default: 0,
    },
    lastFetchDate: {
        type: Date,

    },
    type: {
        type: Number,
        default: 0,
    },
    Task_id: {
        type: Schema.Types.ObjectId,
        index: true,
        ref: 'task'
    }
})
mongoose.model('target', autoFollowTaskSchema);