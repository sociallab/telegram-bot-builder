var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
    return ((this.provider !== 'local' && !this.updated) || property.length);
};

var botSchema = new Schema({
    title: {
        type: String,
        trim: true,
        default: '',
        index: true,
        validate: [validateLocalStrategyProperty, 'لطفا عنوان را وارد کنید']
    },
    bot_id: {
        type: Schema.Types.ObjectId,
        default: null,
    },
    parent: {
        type: Schema.Types.ObjectId,
        default: null,
        index: true
    },
    operation: {
        type: [Schema.Types.Mixed],
        default: [{
            type: 0,
            value: ''
        }]
    },
    enabled: {
        type: Boolean,
        default: true
    },
    provider: {
        type: String,
        required: 'Provider is required'
    },
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('botMenu', botSchema);