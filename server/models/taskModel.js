var mongoose = require('mongoose');

var Schema = mongoose.Schema


let taskSchema = new Schema({
    addAccountBusy: {
        type: Boolean
    },
    AddAccountBusyDate: {
        type: Date,
        default: '',
    },
    app: {
        type: String,
        default: '',
    },
    config: {
        type: Object,
        default: '',
    },
    creationTime: {
        type: Date,
        required: true
    },
    followPattern: {
        type: Array,
    },
    unFollowPattern: {
        type: Array,
    },
    patternId: {
        type: Number,
    },
    isFinish: {
        type: Boolean,
    },
    stop: {
        type: Boolean,
    },
    targets: {
        type: Array,
    },
    user_id: {
        type: Schema.Types.ObjectId,
        default: null,
        index: true,
        ref: 'user'
    },
    login_id: {
        type: Schema.Types.ObjectId,
        default: null,
        index: true,
        ref: 'login'
    }
})
mongoose.model('task', taskSchema);