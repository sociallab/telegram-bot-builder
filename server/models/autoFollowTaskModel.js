var mongoose = require('mongoose');

var Schema = mongoose.Schema


let autoFollowTaskSchema = new Schema({
    followingUsername: {
        type: String,
    },
    followingUserId: {
        type: String,
        index: true
    },
    failFollowCount: {
        type: Number,
        default: 0,
    },
    failUnFollowCount: {
        type: Number,
        default: 0,
    },
    app: {
        type: String,
    },
    creationTime: {
        type: Date,
        required: true
    },
    followDate: {
        type: Date,
        index: true
    },
    UnfollowDate: {
        type: Date,
    },
    followed: {
        type: Boolean,
        index: true
    },
    lastFailErrorMessage: {
        type: String,
    },
    lastFailFollowUtcDate: {
        type: Date,
    },
    lastFailUnFollowUtcDate: {
        type: Date,
    },
    Unfllowed: {
        type: Boolean,
        index: true
    },
    Task_id: {
        type: Schema.Types.ObjectId,
        index: true,
        ref: 'task'
    }
})
mongoose.model('autoFollowTask', autoFollowTaskSchema);