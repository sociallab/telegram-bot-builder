var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function (property) {
    return ((this.provider !== 'local' && !this.updated) || property.length);
};

var botSchema = new Schema({
    title: {
        type: String,
        trim: true,
        default: '',
        validate: [validateLocalStrategyProperty, 'لطفا عنوان ربات را وارد کنید']
    },
    token: {
        type: String,
        trim: true,
        default: '',
        validate: [validateLocalStrategyProperty, 'لطفا توکن ربات را وارد کنید']
    },
    username: {
        type: String,
        trim: true,
        default: ''
    },
    user_id: {
        type: Schema.Types.ObjectId,
        default: null,
        index: true
    },
    menu: {
        type: Object

    },
    task_id: {
        type: Schema.Types.ObjectId,
        default: null,
        index: true
    },
    enabled: {
        type: Boolean,
        default: true
    },
    provider: {
        type: String,
        required: 'Provider is required'
    },
    expire: {
        type: Date
    },
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('bot', botSchema);