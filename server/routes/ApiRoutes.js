"use strict";


var express = require("express");
var botMenuRoutes = require("../routes/botMenuRoutes");
var botRouter = require("../routes/botRouter");
// import AppLoginRoutes from './../routes/appLoginRoutes'
var AppLoginRoutes = require('./../routes/appLoginRoutes');
var app = express();
var ApiRoutes = (function () {
    function Routes() { }
    Object.defineProperty(Routes.prototype, "routes", {
        get: function () {
            app.use("/", new botMenuRoutes().routes);
            app.use("/", new botRouter().routes);
            app.use("/", new AppLoginRoutes().routes);
            return app;
        },
        enumerable: true,
        configurable: true
    });
    return Routes;
} ());
module.exports = ApiRoutes;