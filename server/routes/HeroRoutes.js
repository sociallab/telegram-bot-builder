// /**
//  * Created by Moiz.Kachwala on 15-06-2016.
//  */
// "use strict";
// var express = require("express");
// var HeroController = require("./../controllers/instagramController");
// var router = express.Router();
// var HeroRoutes = (function() {
//     function HeroRoutes() {
//         this._heroController = new HeroController();
//     }
//     Object.defineProperty(HeroRoutes.prototype, "routes", {
//         get: function() {
//             var controller = this._heroController;
//             router.get("/instagram", controller.retrieve);
//             router.post("/instagram", controller.create);
//             router.put("/instagram/:_id", controller.update);
//             router.get("/instagram/:_id", controller.findById);
//             router.delete("/instagram/:_id", controller.delete);
//             return router;
//         },
//         enumerable: true,
//         configurable: true
//     });
//     return HeroRoutes;
// }());
// Object.seal(HeroRoutes);
// module.exports = HeroRoutes;