/**
 * Created by Moiz.Kachwala on 15-06-2016.
 */
"use strict";
var express = require("express");
var botController = require("./../controllers/botController");
var router = express.Router();
var multer = require('multer');
var mkdirp = require('mkdirp');
var path = require('path');
var Constants = require("../config/constants/constants");

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/uploads/' + req.bot._id;
        mkdirp(dest, function (err) {
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

var upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        // if (ext !== '.png' &&
        //  ext !== '.jpg' &&
        //   ext !== '.gif' &&
        //    ext !== '.jpeg'&&
        //    ext !== '.mp3'&&
        //    ext !== '.acc'&&
        //    ext !== '.mp4'&&
        //    ext !== '.mov'&&
        //    ext !== '.mp4'&&
        //    ext !== '.mp4'&&
        //    ext !== '.jpeg'
        //    )
        if (Constants.AllowedFileExtensions.indexOf(ext.toUpperCase()) === -1) {
            console.log(ext.toUpperCase())
            console.log(Constants.AllowedFileExtensions)

            return callback(new Error('شما مجاز به آپلود این نوع فایل نیستید'))
        }
        callback(null, true)
    },
    limits: {
        fileSize: 20000000//6144 * 6144
    }
});

var botRoutes = (function () {
    function botRoutes() {
        this._botController = new botController();
    }
    Object.defineProperty(botRoutes.prototype, "routes", {
        get: function () {
            var controller = this._botController;


            router.get("/bot", controller.retrieve);
            router.post("/bot", controller.create);
            router.get("/bot/getoperations", controller.getTelegramBotOpertions);
            router.get("/bot/test", controller.test);
            router.get("/bot/:botid", controller.getBot);
            router.get("/bot/menu/:botid", controller.getBotMenu);
            router.put("/bot/menu/:botid", controller.saveBotMenu);
            router.post("/bot/task/:botid", controller.setTaskState);
            router.post("/bot/file/:botid", upload.any(), controller.uploud);
            router.delete("/bot/file/:botid", controller.deleteFile);
            router.post("/bot/send/:botid", controller.creatSendTask);
            router.get("/bot/send/:botid", controller.getSendTasks);
            router.get("/bot/user/:botid", controller.getBotUsersList);

            router.param('botid', controller.findById)

            // router.put("/user/:_id", controller.update);
            // router.get("/user/:_id", controller.findById);
            // router.delete("/user/:_id", controller.delete);
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return botRoutes;
} ());
Object.seal(botRoutes);
module.exports = botRoutes;