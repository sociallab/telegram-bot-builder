// import { appLoginController } from './../controllers/appsLoginController'

const appLoginController = require('./../controllers/appLoginController');
const express = require("express");

const router = express.Router();

class AppLoginRoutes {
    constructor() {
        this._appLoginController = new appLoginController();
    }
    get routes() {
        var controller = this._appLoginController;

        // router.get("/applogin", controller.retrieve);
        router.post("/applogin/instagram", controller.instagramCreate);

        return router
    }


}

module.exports = AppLoginRoutes;