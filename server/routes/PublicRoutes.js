"use strict";


var express = require("express");
var publicUserRoutes = require("../routes/publicUserRoutes");
var publicAuthRoutes = require("../routes/publicAuthRoutes");
var app = express();
var publicRoutes = (function() {
    function Routes() {}
    Object.defineProperty(Routes.prototype, "routes", {
        get: function() {
            app.use("/", new publicUserRoutes().routes);
            app.use("/", new publicAuthRoutes().routes);
            return app;
        },
        enumerable: true,
        configurable: true
    });
    return Routes;
}());
module.exports = publicRoutes;