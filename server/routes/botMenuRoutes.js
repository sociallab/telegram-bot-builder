/**
 * Created by Moiz.Kachwala on 15-06-2016.
 */
"use strict";
var express = require("express");
var botController = require("./../controllers/botMenuController");
var router = express.Router();
var botMenuRoutes = (function() {
    function botMenuRoutes() {
        this._botController = new botController();
    }
    Object.defineProperty(botMenuRoutes.prototype, "routes", {
        get: function() {
            var controller = this._botController;
            // router.get("/user", controller.retrieve);
            router.post("/botMenu", controller.create);
            // router.put("/user/:_id", controller.update);
            // router.get("/user/:_id", controller.findById);
            // router.delete("/user/:_id", controller.delete);
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return botMenuRoutes;
}());
Object.seal(botMenuRoutes);
module.exports = botMenuRoutes;