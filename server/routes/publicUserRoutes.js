/**
 * Created by Moiz.Kachwala on 15-06-2016.
 */
"use strict";
var express = require("express");
var publicUserController = require("./../controllers/publicUserController");
var router = express.Router();
var usersRoutes = (function() {
    function usersRoutes() {
        this._publicUserController = new publicUserController();
    }
    Object.defineProperty(usersRoutes.prototype, "routes", {
        get: function() {
            var controller = this._publicUserController;
            // router.get("/user", controller.retrieve);
            router.post("/signup", controller.create);
            // router.put("/user/:_id", controller.update);
            // router.get("/user/:_id", controller.findById);
            // router.delete("/user/:_id", controller.delete);
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return usersRoutes;
}());
Object.seal(usersRoutes);
module.exports = usersRoutes;