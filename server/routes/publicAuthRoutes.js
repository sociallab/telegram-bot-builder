/**
 * Created by Moiz.Kachwala on 15-06-2016.
 */
"use strict";
var express = require("express");
var publicAuthController = require("./../controllers/publicAuthController");
var router = express.Router();
var HeroRoutes = (function() {
    function HeroRoutes() {
        this.publicAuthController = new publicAuthController();
    }
    Object.defineProperty(HeroRoutes.prototype, "routes", {
        get: function() {
            var controller = this.publicAuthController;
            // router.get("/instagram", controller.retrieve);
            router.post("/authenticate", controller.create);
            // router.put("/instagram/:_id", controller.update);
            // router.get("/instagram/:_id", controller.findById);
            // router.delete("/instagram/:_id", controller.delete);
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return HeroRoutes;
}());
Object.seal(HeroRoutes);
module.exports = HeroRoutes;