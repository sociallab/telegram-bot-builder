'use strict';
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jwt-simple');
var mongoose = require('mongoose');


var jwtconfig = require('./jwtconfig').jwtconfig;

var User = mongoose.model('user');

module.exports = function(passport) {
    var opts = {};

    opts.secretOrKey = jwtconfig.secret;
    opts.issuer = jwtconfig.issuer;
    opts.audience = jwtconfig.audience;
    opts.jwtFromRequest = ExtractJwt.fromAuthHeader();

    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        console.log(`PAYLOAD: ${jwt_payload}`);
        User.findOne({ username: jwt_payload.sub }, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                done(null, user);
            } else done(null, false, 'User found in token not found');
        });
    }));
};