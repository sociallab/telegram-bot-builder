
const path = require('path')
var Constants = (function () {
    function Constants() { }
    return Constants;
} ());
Constants.DB_CONNECTION_STRING = process.env.NODE_ENV === 'production' ? process.env.dbURI : "mongodb://127.0.0.1:27017/tablighGram";
Constants.secretKey = '+D3YnB+6w"8V^k%_';
Constants.TelegramBotOpertions = [
    {
        "id": 0,
        "title": "none"
    },
    {
        "id": 1,
        "title": "ارسال متن"
    }, {
        "id": 2,
        "title": "تغییر مسیر"
    },
    {
        "id": 3,
        "title": "ارسال فایل"
    },
    // {
    //     "id": 4,
    //     "title": "ارسال ویدیو"
    // },
    // {
    //     "id": 5,
    //     "title": "ارسال ویدیو"
    // },
    // {
    //     "id": 6,
    //     "title": "ارسال صوت"
    // },
    // {
    //     "id": 7,
    //     "title": "ارسال فایل"
    // },

    {
        "id": 4,
        "title": "ارسال موقعیت مکانی"
    },
    {
        "id": 5,
        "title": "ارسال مخاطب"
    },
]


Constants.AllowedFileExtensions = [
    '.JPG',
    '.JPEG',
    '.PNG',
    '.GIF',
    '.MP4',
    '.MOV',
    '.MP3',
    '.WAV',
    '.AIFF',
    '.AAC',
    '.OGG',
    '.WMA',
    '.RAR',
    '.ZIP',
    '.DOCX',
    '.DOC',
    '.TXT',
    '.SRT',
    '.PDF',
    '.XLSX',
    '.3GP',

]
Constants.getBasePath = path.join(__dirname, './../../../')

Object.seal(Constants);
module.exports = Constants;
