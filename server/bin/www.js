#!/usr/bin/env node

var app = require('../server').app;
var http = require('http');
var Constants = require("../config/constants/constants");
var mongoose = require('mongoose');


connect()
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);

function listen() {
    var server = http.createServer(app);

    server.listen(app.get('port'), function() {
        var host = server.address().address;
        var port = server.address().port;
        console.log('This express angular app is listening on port:' + port);
    });
}

function connect() {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    return mongoose.connect(Constants.DB_CONNECTION_STRING, options).connection;
}