var util = require("util");
var FileCookieStore = require('./../../../tough-cookie-filestore/index');
var path = require('path');
var fs = require('fs');
var _ = require('underscore');
var Helpers = require('../../helpers');
var CookieStorage = require('./cookie-storage');


function CookieFileStorage(loginId) {
    // cookiePath = path.resolve(cookiePath);
    // Helpers.ensureExistenceOfJSONFilePath(cookiePath);
    CookieStorage.call(this, new FileCookieStore(loginId))
}

util.inherits(CookieFileStorage, CookieStorage);
module.exports = CookieFileStorage;


CookieFileStorage.prototype.destroy = function () {
    fs.unlinkSync(this.storage.filePath);
}