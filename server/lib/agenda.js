var Agenda = require('agenda');
var Constants = require("../config/constants/constants");
var fs = require('fs');
var mongoose = require('mongoose');
var moment = require('moment');

console.log(Constants.DB_CONNECTION_STRING);
var join = require('path').join;
var agenda = new Agenda({ db: { address: Constants.DB_CONNECTION_STRING, collection: 'tasks' } });
var models = join(__dirname, './../models');
fs.readdirSync(models)
    .filter(file => ~file.search(/^[^\.].*\.js$/))
    .forEach(file => {
        require(join(models, file));
        console.log(file);
        console.log(join(models, file));
    }

    );

function mongooseConnect() {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    return mongoose.connect(Constants.DB_CONNECTION_STRING, options).connection;
}

var jobTypes = process.argv[2] ? process.argv[2].split(',') : [];
// var jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [];
console.log(jobTypes);
jobTypes.forEach(function (type) {
    require('./../jobs/' + type)(agenda);
})

function removeStaleJobs(callback) {
    agenda._collection.update({
        lockedAt: {
            $exists: true
        }
    }, {
            $set: {
                lockedAt: null
            }
        }, {
            multi: true
        }, callback);
}

agenda.on('ready', function () {
    if (jobTypes.length) {
        mongooseConnect()
            .on('error', console.log)
            .on('disconnected', mongooseConnect)
            .once('open', function () {
           
                removeStaleJobs(function (e, r) {
                    if (e) {
                        console.error("Unable to remove stale jobs. Starting anyways.");
                    }
                    agenda.every('10 minutes', 'removeStaleJobs');

                    agenda.start();
                });

            });
    }
});
agenda.on('fail', (err, job) => {
    // isErrorTemporary(err)
    if (true) { // checking that the error is a network error for instance
        job.attrs.nextRunAt = moment().add(10000, 'milliseconds').toDate(); // retry 10 seconds later
        job.save();
    }
});
agenda.define('removeStaleJobs', function (job, done) {

    agenda._collection.update({
        lockedAt: {
            $exists: true
        }
    }, {
            $set: {
                lockedAt: null
            }
        }, {
            multi: true
        }, (err) => {
            if (err) {
                console.error("Unable to remove stale jobs. Starting anyways.");
            }
        });
});

// if (jobTypes.length) {
//     agenda.start();
// }



// Handles graceful stopping of jobs
function graceful() {
    agenda.stop(function () {
        process.exit(0);
    });
}

process.on('SIGTERM', graceful);
process.on('SIGINT', graceful);


module.exports = agenda;