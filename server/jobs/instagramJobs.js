var mongoose = require('mongoose');
var taskModel = mongoose.model('task');
var loginModel = mongoose.model('login');
var autoFollowTaskModel = mongoose.model('autoFollowTask');
var targetModel = mongoose.model('target');
var ObjectId = mongoose.Types.ObjectId;
var moment = require('moment');
const Promise = require("bluebird");
var Constants = require("./../config/constants/constants");
const _ = require('lodash');
var instaClient = require('instagram-private-api').V1;

function handleError(err) {

}

function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}
module.exports = function (agenda) {
    agenda.define('instagram AutoFollow', function (job, done) {
        let TaskId = job.attrs.data.token;
        let UserId = job.attrs.data.UserId;

        let task = await taskModel.findById(TaskId);

        var login = await loginModel.findOne({
            user_id: UserId
        });
        //   if (err) {
        //         console.error(err);
        //         handleError(err);
        //     }

        if (task.Stop) {
            done();
            return;
        }

        let config = JSON.parse(task.config);

        var date = new Date();
        let currentMinute = d.getUTCMinutes();
        let currentHour = d.getUTCHours();

        let follows = new Array();
        let unFollows = new Array();

        if (UserTask.PatternId != currentHour || UserTask.MustReset) {

            let freeIndexes = _.range(currentMinute, 60 - currentMinute);
            follows.push(...shuffle(freeIndexes).slice(0, config.followPerHour))
            unFollows.push(...shuffle(freeIndexes).slice(0, config.followPerHour))

            task.followPattern = follows;
            task.unFollowPattern = unFollows;
            var Promise = await task.save()



            // .then(() => {

            // })
            // .catch((err) => {
            //     console.log(err);
            //     done();
            //     return;
            // });

        }
        if (task.followPattern.includes(currentMinute)) {
            // autoFollowTaskModel.find({Task_id:TaskId},{followed:})
            var item = await autoFollowTaskModel.
            find().
            where('Task_id').equals(TaskId).
            where('followed').equals(true). //Additional where query
            where('failFollowCount').lte(retryCount).
            sort({
                age: -1
            }).
            limit(1).
            exec();
            new Date().getUTCFullYear
            //fetch an account to follow
            if (TaskAccount == null) //&& !(await _userTaskService.Get(userTaskId)).AddAccountBusy
            {
                let targets = await targetModel.find({
                    Task_id: TaskId
                }).sort({
                    lastFetchDate: 'asc'
                });
                if (targets === null || target.length < 1) {
                    return;
                }
                let target = targets[0];
                var freshUT = await taskModel.findById(TaskId);
                if (freshUT.addAccountBusy && ((Date().now() - freshUT.AddAccountBusyDate) / (1000 * 3600)) < 2) {
                    return;
                }
                freshUT.addAccountBusy = true;
                await freshUT.save();
                target.lastFetchDate = Date.now();
                await target.save();
                switch (target.type) {
                    case 0:
                        { //page-Followers

                            instaClient.

                            break;
                        }



                    default:
                        break;
                }
            }
        }

    })

};