// var mongoose = require('mongoose');
// var bots = require('../models/botModel.js');
var mongoose = require('mongoose');
var botModel = mongoose.model('bot');
var telegramUserBot = mongoose.model('telegramUserBot');
var ObjectId = mongoose.Types.ObjectId;
var TelegramBot = require('node-telegram-bot-api');
var response = require('./Telegram/telegramBotResponse');
var moment = require('moment');
const Promise = require("bluebird");
var Constants = require("./../config/constants/constants");

var _offset = 0;


module.exports = function (agenda) {
  agenda.define('telegram bot', function (job, done) {
    //   User.remove({lastLogIn: { $lt: twoDaysAgo }}, done);
    // console.log(job.attrs.data.token)

    try {
      var bot = new TelegramBot(job.attrs.data.token, { polling: false })

      bot.getUpdates(10, 100, _offset + 1).then(function (updates) {
        if (updates.length > 0) {
          _offset = updates[updates.length - 1].update_id
          if (updates[0].message) {
            response(updates[0], done, job.attrs.data.bot_id, bot);

            // bot.sendMessage(updates[0].message.chat.id, "Received your message 0");
          } else {
            done();
          }

        } else {
          done();
        }
        // done();
      }).catch((err) => {
        console.log(err)
        done();
      })
    } catch (error) {
      done();
    }
    // isr()
  });

  agenda.define('telegramBot send', function (job, done) {
    botModel.findById(job.attrs.data.bot_id, (err, bot) => {
      if (err) {
        console.log(err);
        job.fail(new Error('در واکشی کاربران ربات مشکلی پیش آمده است.'));
        job.save();
        job.done();

      } else {
        var bot = new TelegramBot(bot.token, { polling: false });


        telegramUserBot.find({ bot_id: job.attrs.data.bot_id }, (err, users) => {
          let counter = 0;
          if (err) {
            console.log(err);
            job.fail(new Error('در واکشی کاربران ربات مشکلی پیش آمده است.'));
            job.save();
            job.done();

          } else {
            for (let user of users) {
              try {
                //for (let send of job.attrs.data.sendList) {
                Promise.mapSeries(job.attrs.data.sendList, (send) => {
                  try {
                    switch (send.type) {
                      case "1": {
                        return bot.sendMessage(user.telegramUserId, send.content.text);

                      } case "2": {
                        let basePath = `${Constants.getBasePath}public/uploads/${botId}`;
                        let files = send.content.files;
                        for (let item of files) {
                          if (S(item.type).contains('image')) {
                            console.log('image');
                            return bot.sendPhoto(user.telegramUserId, `${basePath}/${item.filename}`);
                          } else if (S(item.type).contains('audio/ogg')) {
                            console.log('voice');
                            return bot.sendVoice(user.telegramUserId, `${basePath}/${item.filename}`);
                          } else if (S(item.type).contains('audio')) {
                            console.log('audio');
                            return bot.sendAudio(user.telegramUserId, `${basePath}/${item.filename}`);
                          }
                          else if (S(item.type).contains('video')) {
                            console.log('video');
                            return bot.sendVideo(user.telegramUserId, `${basePath}/${item.filename}`);
                          } else {
                            console.log('document');
                            return bot.sendDocument(user.telegramUserId, `${basePath}/${item.filename}`);
                          }
                        }
                        break;
                      }
                      case "3": {
                        break;
                        // found = false;
                        // foundValue = undefined;
                        // findById(_menu, selected_node.data.operations[i].content.redirect);
                        // if (foundValue === undefined) {
                        //   gotoMenu(user.telegramUserId, _menu, user, BotSession, col);
                        //   done();
                        //   break;
                        // }

                        // user.state = foundValue.data.id
                        // user.save(function (err) {
                        //   if (err) {
                        //     gotoMenu(user.telegramUserId, _menu, user, BotSession, col);
                        //     done();
                        //     break;
                        //   } else {
                        //     BotSession.sendMessage(user.telegramUserId, foundValue.label, {
                        //       reply_markup: {
                        //         keyboard: keyboardBuilder(foundValue.children, col),
                        //         resize_keyboard: true,
                        //         one_time_keyboard: false,
                        //         // selective: false
                        //       }
                        //     });
                        //     break;
                        //   }
                        // })
                      }
                      case "4": {
                        return bot.sendLocation(user.telegramUserId, send.content.latitude, send.content.Longitude);
                      } case "5": {
                        return bot.sendContact(user.telegramUserId, send.content.contactPhoneNumber,
                          send.content.contactFirstName, { last_name: send.content.contactLastName });
                        // break;
                      }

                      default:
                        break;
                    }
                  } catch (error) {
                    console.log(error);
                    console.log('qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');
                  }
                }).catch(function (error) {
                  console.log(error);
                  console.log('qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');
                });;
              } catch (error) {
                console.log(error);
              }
              counter++;
              job.attrs.data.sent = counter;
              job.save();
            }
            done();
          }


        });
      }
    })

  })

  // More email related jobs
}