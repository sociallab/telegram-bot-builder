
var mongoose = require('mongoose');
var telegramUserBot = mongoose.model('telegramUserBot');
var botmodel = mongoose.model('bot');
var ObjectId = mongoose.Types.ObjectId;
var _ = require('lodash');
var S = require('string');
var utilities = require('./../../lib/utilities')
const Promise = require("bluebird");
var Constants = require("./../../config/constants/constants");

var items = [
    [{ text: '1' }, { text: '1' }, { text: '1' }],
    [{ text: '2' }, { text: '2' }, { text: '2' }],
    [{ text: '3' }, { text: '3' }, { text: '3' }]
];
function handleError(err) {
    console.log('errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr');
    console.log(err);
    done();
}

function keyboardBuilder(menu, col) {
    var ocupied = 0;
    var counter = 0;
    var Maincounter = 0;
    var row = Math.ceil(menu.length / col);
    var keyboard = new Array(row);

    // for (var i = 0; i < items.length; i++) {
    //     x[i] = new Array(3);
    // }

    for (var i = 0; i < row; i++) {
        var tmp = (menu.length - ocupied >= col) ? col : menu.length - ocupied;

        var TmpStringArry = new Array(tmp);
        for (var j = ocupied; j < tmp + ocupied; j++) {

            TmpStringArry[counter++] = { text: menu[j].label };

            if (counter == col || j == tmp + ocupied - 1) {
                keyboard[Maincounter++] = TmpStringArry;
                counter = 0;
            }

        }
        ocupied += tmp;
    }
    return keyboard;

    // x[5][12] = 3.0;
}
var foundValue;
var found;

function findByLabel(tree, label) {

    if (tree.label === label) {
        console.log(tree);
        console.log(tree.data.id);
        found = true;
        foundValue = tree;
        return;
    } else {
        for (var i in tree.children) {
            // avoid circular reference infinite loop & skip inherited properties
            if (tree.children.length === 0) continue;

            findByLabel(tree.children[i], label);
            if (found === true) return;
        }
    }
}
function findById(tree, id) {

    if (tree.data.id === id) {
        console.log(tree);
        console.log(tree.data.id);
        found = true;
        foundValue = tree;
        return;
    } else {
        for (var i in tree.children) {
            // avoid circular reference infinite loop & skip inherited properties
            if (tree.children.length === 0) continue;

            if (tree.children[i])
                findById(tree.children[i], id);
            if (found === true) return;
        }
    }
}
function gotoMenu(_userId, _menu, user, BotSession, col) {
    try {
        user.state = 'main'
        user.save(function (err) {
            console.error(err);
        })
        BotSession.sendMessage(_userId, "منو", {
            reply_markup: {
                keyboard: keyboardBuilder(_menu.children, col),
                resize_keyboard: true,
                one_time_keyboard: false,
                // selective: false
            }
        }).catch((err) => {
            handleError(err);
        });
    } catch (error) {
        handleError(error)
    }


}

function exec(_userId, _menu, user, BotSession, selected_node, done, botId, col) {
    if (selected_node.children.length > 0) {
        BotSession.sendMessage(_userId, 'لطفاً انتخاب کنید', {
            reply_markup: {
                keyboard: keyboardBuilder(selected_node.children, 3),
                resize_keyboard: true,
                one_time_keyboard: false,
                // selective: false
            }
        }).catch((err) => {
            handleError(err);
        });
    }

    // iterate over 'arr_photos' in series
    Promise.mapSeries(selected_node.data.operations, (operation) => {
        switch (operation.type) {

            case "1": {
                if (operation.content.text.length <= 4096)
                    return BotSession.sendChatAction(_userId, 'typing').then(() => {
                        return BotSession.sendMessage(_userId, operation.content.text).catch((err) => {
                            handleError(err);
                        });
                    }).catch((err) => {
                        handleError(err);
                    });

                else {
                    //  throw new Error('something bad happened');

                    let chunks = utilities.chunkString(operation.content.text, 4096);

                    Promise.each(chunks, chunk => {
                        return BotSession.sendChatAction(_userId, 'typing').then(() => {
                            return BotSession.sendMessage(_userId, chunk).catch((err) => {
                                handleError(err);
                            });
                        }).catch((err) => {
                            handleError(err);
                        });
                    });


                }
                break;
            } case "2": {

                found = false;
                foundValue = undefined;
                findById(_menu, operation.content.redirect);
                if (foundValue === undefined) {
                    return gotoMenu(_userId, _menu, user, BotSession, col);
                    // done();
                    // break;
                }

                user.state = foundValue.data.id
                user.save(function (err) {
                    if (err) {
                        return gotoMenu(_userId, _menu, user, BotSession, col);
                        // done();
                        // break;
                    } else {
                        return BotSession.sendMessage(_userId, foundValue.label, {
                            reply_markup: {
                                keyboard: keyboardBuilder(foundValue.children, col),
                                resize_keyboard: true,
                                one_time_keyboard: false,
                                // selective: false
                            }
                        }).catch((err) => {
                            handleError(err);
                        });

                    }
                })
                break;
            }
            case "3": {
                let basePath = `${Constants.getBasePath}public/uploads/${botId}`;
                let files = operation.content.files;
                for (let item of files) {
                    if (S(item.type).contains('image')) {
                        console.log('image');
                        return BotSession.sendChatAction(_userId, 'upload_photo').then(() => {

                            let sendChatActionTimer = setTimeout(function () {
                                BotSession.sendChatAction(_userId, 'upload_photo')
                            }, 5 * 1000);

                            return BotSession.sendPhoto(_userId, `${basePath}/${item.filename}`).then(() => {
                                clearTimeout(sendChatActionTimer);
                            }).catch((err) => {
                                handleError(err);
                            });
                        }).catch((err) => {
                            handleError(err);
                        });

                    } else if (S(item.type).contains('audio/ogg')) {
                        console.log('voice');

                        return BotSession.sendChatAction(_userId, 'record_audio').then(() => {

                            let sendChatActionTimer = setTimeout(function () {
                                BotSession.sendChatAction(_userId, 'record_audio')
                            }, 5 * 1000);
                            return BotSession.sendVoice(_userId, `${basePath}/${item.filename}`).then(() => {
                                clearTimeout(sendChatActionTimer);
                            }).catch((err) => {
                                handleError(err);
                            });
                        }).catch((err) => {
                            handleError(err);
                        });
                    } else if (S(item.type).contains('audio')) {
                        console.log('audio');

                        return BotSession.sendChatAction(_userId, 'upload_audio').then(() => {
                            let sendChatActionTimer = setTimeout(function () {
                                BotSession.sendChatAction(_userId, 'upload_audio')
                            }, 5 * 1000);
                            return BotSession.sendAudio(_userId, `${basePath}/${item.filename}`).then(() => {
                                clearTimeout(sendChatActionTimer);
                            }).catch((err) => {
                                handleError(err);
                            });
                        }).catch((err) => {
                            handleError(err);
                        });
                    }
                    else if (S(item.type).contains('video')) {

                        console.log('video');
                        return BotSession.sendChatAction(_userId, 'upload_video').then(() => {
                            let sendChatActionTimer = setTimeout(function () {
                                BotSession.sendChatAction(_userId, 'upload_video')
                            }, 5 * 1000);
                            return BotSession.sendVideo(_userId, `${basePath}/${item.filename}`).then(() => {
                                clearTimeout(sendChatActionTimer);
                            }).catch((err) => {
                                handleError(err);
                            });
                        }).catch((err) => {
                            handleError(err);
                        });
                    } else {
                        console.log('document');

                        return BotSession.sendChatAction(_userId, 'upload_document').then(() => {
                            let sendChatActionTimer = setTimeout(function () {
                                BotSession.sendChatAction(_userId, 'upload_document')
                            }, 5 * 1000);
                            return BotSession.sendDocument(_userId, `${basePath}/${item.filename}`).then(() => {
                                clearTimeout(sendChatActionTimer);
                            }).catch((err) => {
                                handleError(err);
                            });
                        }).catch((err) => {
                            handleError(err);
                        });
                    }
                }
                break;

            }
            case "4": {
                return BotSession.sendLocation(_userId, operation.content.latitude, operation.content.Longitude).catch((err) => {
                    handleError(err);
                });
                // break;
            } case "5": {
                return BotSession.sendContact(_userId, operation.content.contactPhoneNumber,
                    operation.content.contactFirstName, { last_name: operation.content.contactLastName }).catch((err) => {
                        handleError(err);
                    });
                // break;
            }

            // break;

            default:
                break;
        }
    }).then(function () {
        // after all messages have been sent successfully
    }).catch(function (error) {
        // handle error!
        handleError(error)
    });

    for (var i in selected_node.data.operations) {

    }
}

module.exports = function response(update, done, botId, BotSession) {

    var _userId = update.message.from.id;
    // if (!mongoose.Types.ObjectId.isValid(botId)) {
    //     console.log('NotValid')
    // }
    if (update.message) {
        botmodel.findById(ObjectId(botId), function (err, bot) {
            if (err) handleError(err)
            else {
                var _menu = {
                    data: { id: "main" },
                    label: 'main',
                    children: bot.menu.data
                };

                telegramUserBot.UserExistsInBot(update.message.from.id, botId, function (err, user) {
                    if (!err) {
                        if (!user) {
                            var tUser = new telegramUserBot({
                                telegramUserId: _userId,
                                bot_id: botId,
                                state: 'main'
                            })
                            tUser.save(function (err) {
                                if (err) handleError(err);
                                else {
                                    BotSession.sendMessage(_userId, "خوش‌آمدید",
                                        {
                                            reply_markup: {
                                                keyboard: keyboardBuilder(_menu.children, 3),
                                                resize_keyboard: true,
                                                one_time_keyboard: false,
                                                // selective: false
                                            }
                                        }).catch(function (error) {
                                            // handle error!
                                            handleError(error)
                                        });;

                                    done();
                                }

                            })
                        } else {

                            try {
                                found = false;
                                foundValue = undefined;
                                findById(_menu, user.state);
                                if (foundValue === undefined) {
                                    gotoMenu(_userId, _menu, user, BotSession, 3);
                                    done();
                                }
                                var selected_node = _.find(foundValue.children, { label: update.message.text })

                                if (selected_node === undefined) {
                                    gotoMenu(_userId, _menu, user, BotSession, 3);
                                    done();

                                }

                                if (selected_node.children.length) {
                                    user.state = selected_node.data.id;
                                    user.save(function (err) {
                                        if (err) {
                                            gotoMenu(_userId, _menu, user, BotSession, 3);
                                            done();
                                        } else {
                                            exec(_userId, _menu, user, BotSession, selected_node, done, botId, 3)
                                            done();
                                        }
                                    })
                                } else {
                                    exec(_userId, _menu, user, BotSession, selected_node, done, botId, 3)
                                    done();
                                }

                            } catch (error) {
                                gotoMenu(_userId, _menu, user, BotSession, 3);
                                console.log(error)
                                done();

                            }
                        }
                    } else {
                        gotoMenu(_userId, _menu, user, BotSession, 3);
                        handleError(err);
                        done();
                    }
                })
            }

        })


    }

}