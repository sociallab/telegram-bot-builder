var mongoose = require('mongoose');
var autoFollowTaskModel = mongoose.model('autoFollowTask');
var taskModel = mongoose.model('task');

module.exports = function (taskId, retryCount, username, loginId, done) {
    autoFollowTaskModel.find({ Task_id: taskId }).
        where('followed').
        equals(false).
        where('failUnFollowCount').lte(retryCount).
        sort('failUnFollowCount').limit(1).exec().then((res) => {
            if (res.length > 0) {
                var storage = new instaClient.CookieFileStorage(loginId);
                var device = new instaClient.Device(username);

                instaClient.Session.create(device, storage).
                    catch(function (err) {
                        console.log(err);
                        done();
                        return;
                    })
                    .then(function (session) {
                        return [session, instaClient.Relationship.create(session, res[0].followingUserId)];
                    }).spread((session, relationship) => {
                        if (relationship.params.following) {
                            res[0].followed = true;
                            res[0].followDate = new Date();
                            res[0].save().then(() => {
                                if (!relationship.params.isPrivate) {
                                    var feed = new Client.Feed.UserMedia(session, '4072808008', 10);
                                    return [session, feed.get()]
                                } else {

                                    done;
                                    return;
                                }




                            }).catch((err) => {
                                console.log(err);
                                done();
                                return;
                            })
                        }
                    }).
                    catch(function (err) {
                        console.log(err);
                        res[0].lastFailErrorMessage = err;
                        res[0].lastFailFollowUtcDate = new Date();
                        res[0].failFollowCount++;
                        res[0].save().then(() => {
                            done();
                        }).catch((err) => {
                            console.log(err);
                            done();
                            return;
                        })
                        done();
                        return;
                    }).spread((session, medias) => {

                        if (medias.length > 0 && !relationship.params.isPrivate) {
                            var length = Math.floor((Math.random() * 4) + 0);
                            for (let i = 0; i < length; i++) {
                                instaClient.Like.create(session, medias[Math.floor((Math.random() * medias.length) + 0)].id).catch((err) => {
                                    console.log(err);
                                })
                            }
                        }

                    }).catch((err) => {
                        console.log(err);
                        done();
                    })


            } else {

            }
        }).catch((err) => {
            console.log(err);
            done();
            return;
        })
}