"use strict";


var path = require('path'),
    mongoose = require('mongoose'),
    botMenuModel = mongoose.model('botMenu');
// errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));



var botMenuController = (function() {
    function botMenuController() {}
    botMenuController.prototype.create = function(req, res) {

        // res.json({ success: false, msg: 'لطفا فیلد‌های ضروری را وارد نمایید.' });

        var botMenu = new botMenuModel({
            title: req.body.title,
            bot_id: req.body.bot_id,
            operation: req.body.operation,
            enabled: req.body.enabled,
            parent: req.body.parent,
        });
        botMenu.provider = 'local';

        console.log(botMenu);

        botMenu.save(function(err) {
            if (err) {
                console.log(err);
                return res.json({ success: false, msg: err });
            }
            res.json({ success: true, msg: 'Successfully created a Bot Menu item!' });
        });

    };

    return botMenuController;
}());
module.exports = botMenuController;