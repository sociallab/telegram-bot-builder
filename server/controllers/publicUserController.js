"use strict";


var path = require('path'),
    mongoose = require('mongoose'),
    User = mongoose.model('user');
// errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));



var publicUserController = (function() {
    function publicUserController() {}
    publicUserController.prototype.create = function(req, res) {


        if (!req.body.username || !req.body.password) {
            res.json({ success: false, msg: 'لطفا فیلد‌های ضروری را وارد نمایید.' });
        } else {
            var newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                username: req.body.username,
                password: req.body.password,
                email: req.body.email,
            });
            newUser.provider = 'local';
            newUser.displayName = newUser.firstName + ' ' + newUser.lastName;

            console.log(newUser);
            newUser.save(function(err) {
                if (err) {
                    console.log(err);
                    return res.json({ success: false, msg: err });
                }
                res.json({ success: true, msg: 'Successfully created a User!' });
            })
        }
    };
    return publicUserController;
}());

module.exports = publicUserController;