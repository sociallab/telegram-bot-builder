"use strict";


var path = require('path'),
    mongoose = require('mongoose'),
    User = mongoose.model('user');
// errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

var jwtconfig = require('../config/jwtconfig').jwtconfig;
var jwt = require('jwt-simple');

var publicAuthController = (function() {
    function publicAuthController() {}
    publicAuthController.prototype.create = function(req, res) {
        User.findOne({
            username: req.body.username
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.status(401).send({ msg: 'Authentication failed. User not found' });
            } else {
                if (user.authenticate(req.body.password)) {
                    var iat = new Date().getTime() / 1000;
                    var exp = iat + jwtconfig.tokenExpirationTime;
                    var payload = {
                        aud: jwtconfig.audience,
                        iss: jwtconfig.issuer,
                        iat: iat,
                        exp: exp,
                        sub: user.username,
                        ui: user._id
                    };
                    var token = jwt.encode(payload, jwtconfig.secret);
                    res.json({ token: 'JWT ' + token });
                } else {
                    res.status(401).send({ msg: 'Authentication failed. Wrong password' });
                }

            }

        })
    };

    return publicAuthController;
}());
module.exports = publicAuthController;