"use strict";


const path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    botModel = mongoose.model('bot'),
    telegramUserBot = mongoose.model('telegramUserBot'),
    agenda = require('./../lib/agenda'),
    ObjectId = mongoose.Types.ObjectId;
const MongoObjectId = require('mongodb').ObjectId;



// var mime = require('mime-types')
// var S = require('string');


// var Agenda = require('agenda');
var Constants = require("../config/constants/constants");

// var agenda = new Agenda({ db: { address: Constants.DB_CONNECTION_STRING, collection: 'tasks' } });


// errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));



var botController = (function () {
    function botController() { }
    botController.prototype.create = function (req, res) {

        // res.json({ success: false, msg: 'لطفا فیلد‌های ضروری را وارد نمایید.' });

        var job = agenda.create('telegram bot', { token: req.body.token, stop: true });
        job.disable()
        job.save(function (err) {
            if (err) {
                console.log(err);
                return res.json({ success: false, msg: err });
            }

            var bot = new botModel({
                title: req.body.title,
                user_id: req.user._id,
                token: req.body.token,
                username: req.body.username,
                task_id: job.attrs._id
            });
            bot.provider = 'local';

            console.log(bot);

            bot.save(function (err) {
                if (err) {
                    console.log(err);
                    try {
                        job.remove();
                    } catch (error) {
                        console.log(error);
                    }
                    return res.json({ success: false, msg: err });
                }
                job.attrs.data.bot_id = bot._id;
                bot.save(function (err) {
                    if (!err) {
                        res.json({ success: true, msg: 'Successfully created a Bot!' });
                    } else {
                        try {
                            job.remove();
                        } catch (error) {
                            console.log(error);
                        }
                        try {
                            bot.remove();
                        } catch (error) {
                            console.log(error);
                        }

                        return res.json({ success: false, msg: err });
                    }
                })

            });
        });



    };

    botController.prototype.retrieve = function (req, res) {


        botModel.find({ user_id: req.user._id }, { provider: 0, menu: 0 }, (err, bots) => {
            if (err) {
                // Send the error to the client if there is one
                return res.json({ success: false, msg: err });
            }
            // Send todos in JSON format
            res.json(bots);
        });

    };
    botController.prototype.saveBotMenu = function (req, res) {
        var bot = req.bot;
        bot.menu = req.body;

        bot.save(function (err) {
            if (err) {
                // Send the error to the client if there is one
                return res.status(422).send({ success: false, msg: err });
            }
            return res.json({ success: true, menu: bot.menu });

        })
    };
    botController.prototype.getBotMenu = function (req, res) {
        var obj = {
            data: []
        }
        try {
            if (typeof req.bot.menu !== 'object')
                res.json(obj);
            else
                res.json(req.bot.menu);
        } catch (err) {

            res.json(obj);

        }

    };
    botController.prototype.getBot = function (req, res) {
        res.json(req.bot);
    };
    botController.prototype.getTelegramBotOpertions = function (req, res) {

        res.json(Constants.TelegramBotOpertions)

    };
    botController.prototype.test = function (req, res) {

        agenda.every('one seconds', 'registration email', { botId: req.query.botId });



    };
    // botController.prototype.startTask = function (req, res) {
    //     agenda.jobs({ _id: ObjectId(req.body.task_id) }, function (err, jobs) {
    //         if (jobs.length) {
    //             jobs[0].repeatEvery('1 seconds');
    //             jobs[0].attr.data.stop = false;
    //             jobs[0].enable();
    //             jobs[0].save();
    //         }
    //     });
    // };
    // botController.prototype.stopTask = function (req, res) {
    //     agenda.jobs({ _id: ObjectId(req.body.task_id) }, function (err, jobs) {
    //         if (jobs.length) {
    //             jobs[0].attr.data.stop = true;
    //             jobs[0].disable();
    //             jobs[0].save();
    //         }
    //     });
    // };
    botController.prototype.setTaskState = function (req, res) {
        if (req.body.state) {
            agenda.jobs({ _id: MongoObjectId(req.body.taskId) }, function (err, jobs) {
                if (jobs.length) {
                    jobs[0].repeatEvery('1 seconds');
                    jobs[0].attrs.data.stop = false;
                    jobs[0].enable();
                    jobs[0].save(function (err) {
                        if (err) {
                            return res.status(500).send({ success: false, msg: err });
                        }
                        return res.json({ success: true, state: 'started' });
                    });
                } else {
                    return res.status(404).send({
                        msg: 'No task with that identifier has been found'
                    });
                }
            });
        } else {
            agenda.jobs({ _id: MongoObjectId(req.body.taskId) }, function (err, jobs) {
                if (jobs.length) {
                    jobs[0].repeatEvery('1 minutes');

                    jobs[0].attrs.data.stop = true;
                    jobs[0].disable();
                    jobs[0].save(function (err) {
                        if (err) {
                            return res.status(500).send({
                                msg: { success: false, msg: err }
                            });
                        }
                        return res.json({ success: true, state: 'stopped' });
                    });
                }
            });
        }
    };

    botController.prototype.creatSendTask = function (req, res) {
        // console.log(res);
        var job = agenda.now('telegramBot send', { bot_id: req.bot._id.toString(), sendList: req.body.sendList });
        console.log('job created....');
        // job.save();
        console.log(job);
        return res.json({ success: true, msg: 'دستور ارسال با موفقیت ثبت شد. ارسال در حال انجام می باشد.' });
    };
    botController.prototype.getSendTasks = function (req, res) {

            agenda.jobs({ 'data.bot_id': req.body.bot_id },{}, function (err, jobs) {
              return res.json(jobs);
            });
     
    };
    botController.prototype.getBotUsersList = function (req, res) {
            telegramUserBot.find({ 'bot_id': req.bot._id }, function (err, jobs) {
              return res.json(jobs);
            });
    };

    botController.prototype.scheduleTask = function (req, res) {
        agenda.jobs({ _id: ObjectId('58810ad387b1c1bc0a91a648') }, function (err, jobs) {

            jobs[0].enable();
            jobs[0].save();
        });

    };

    botController.prototype.uploud = function (req, res) {
        console.log(req.body);
        res.send(req.files);

        // for (var i in req.files) {

        //     req.files[i].
        // }
    };
    botController.prototype.deleteFile = function (req, res) {

        let filePath = `${Constants.getBasePath}public/uploads/${req.bot._id}/${req.body.filename}`;

        console.log(`Hello, ${"x"}, file path is ${filePath}, Today is: ${new Date()}`)
        fs.stat(filePath, function (err, stats) {
            console.log(stats);//here we got all information of file in stats variable

            if (err) {
                console.log(err)
                return res.status(200).send({
                    success: false,
                    msg: 'No file with that identifier has been found'
                });
            }

            fs.unlink(filePath, (err) => {
                console.log(err);
                if (err) return res.status(200).send({
                    success: false,
                    msg: 'فایل حذف نشد!'
                });

                res.json({
                    success: true,
                    msg: 'فایل با موفقیت حذف شد'
                })
                console.log('successfully deleted /tmp/hello');

            });

        });



    };

    botController.prototype.findById = function (req, res, next, id) {

        if (!mongoose.Types.ObjectId.isValid(id)) {
            return res.status(400).send({
                msg: 'bot is invalid'
            });
        }

        botModel.findById(id, function (err, bot) {
            if (err) {
                return next(err);
            } else if (!bot) {
                return res.status(404).send({
                    success: false,
                    msg: 'No bot with that identifier has been found'
                });
            }

            if (req.user._id.toString() != bot.user_id.toString()) {
                return res.status(403).send({
                    msg: { success: false, msg: 'شما اجازه دسترسی به این ربات را ندارید' }
                });
            } else {
                req.bot = bot;
                next();
            }

        })

    };


    return botController;
} ());
module.exports = botController;