var mongoose = require('mongoose')
var loginModel = mongoose.model('login');
var instaClient = require('instagram-private-api').V1;
var path = require("path")

class appLoginController {
    constructor() {
        this._that = this;
    }
    rollback(login) {
        loginModel.remove(login, (err) => {
            consol.log(err)
        })
    }
    async instagramCreate(req, res) {
        try {
            if (req.body.app === 'instagram') {
                var login = new loginModel({
                    cookie: 'null',
                    app: 'instagram',
                    username: req.body.username,
                    user_id: req.user._id
                })
                login.save((err, savedLogin) => {
                    if (err) {
                        console.log(err);
                        return res.status(200).send({
                            success: false,
                            msg: '.خطایی در انجام عملیا رخ داده است!'
                        });
                    }

                    // let loginPath = __dirname + './../appLogins/' + req.body.username + '_' + savedLogin._id.toString() + '.json';
                    let loginPath = path.join(__dirname, './../appLogins/', req.body.username + '_' + savedLogin._id.toString() + '.json');



                    var device = new instaClient.Device(req.body.username);
                    var storage = new instaClient.CookieFileStorage(loginPath);

                    instaClient.Session.create(device, storage, req.body.username, req.body.password)
                        .then(async function (session) {
                            try {
                                savedLogin.path = loginPath;
                                await savedLogin.save()
                                return res.status(200).send({
                                    success: true,
                                    msg: '.لاگین با موفقیت ایجاد شد'
                                });
                            } catch (error) {

                                console.log(error);

                                return res.status(200).send({
                                    success: false,
                                    msg: '.خطایی در انجام عملیا رخ داده است!'
                                });
                            }


                        }).catch(function (err) {
                            console.log(err);
                            // this._that.rollback(savedLogin);
                            loginModel.remove(login, (err) => {
                                if (err) {
                                    console.log(err);
                                }
                            })
                            if (err.name === 'AuthenticationError') {

                                return res.status(200).send({
                                    success: false,
                                    msg: '.نام کاربری یا کلمه عبور شما اشتباه می‌باشد!'
                                });
                            }
                            return res.status(200).send({
                                success: false,
                                msg: '.خطایی در انجام عملیا رخ داده است!'
                            });

                        })


                })
            } else {
                return res.status(200).send({
                    success: false,
                    msg: '.خطایی در انجام عملیا رخ داده است!'
                });
            }
        } catch (error) {
            console.log(error);
            // _that.rollback(savedLogin);
            loginModel.remove(login, (err) => {
                consol.log(err)
            })
            return res.status(200).send({
                success: false,
                msg: '.خطایی در انجام عملیا رخ داده است!'
            });
        }

    };
}


module.exports = appLoginController;