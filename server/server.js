
var fs = require('fs');
var express = require('express');
var join = require('path').join;
var bodyParser = require("body-parser");
var models = join(__dirname, './models');
var path = require('path');
var port = process.env.PORT || 3000;
var env = process.env.NODE_ENV || 'developement';
var helmet = require('helmet');
var passport = require('passport');

fs.readdirSync(models)
    .filter(file => ~file.search(/^[^\.].*\.js$/))
    .forEach(file => {
            require(join(models, file));
            console.log(file);
            console.log(join(models, file));
        }
    );


var apiRoutes = require("./routes/apiRoutes");
var publicRoutes = require("./routes/publicRoutes");


var app = express();
exports.app = app;

var passportConfig = require('./config/passport');

passportConfig(passport);

app.set('port', port);

app.use('/', express.static(path.resolve(__dirname, '../dist')));
app.use('/public', express.static(path.resolve(__dirname, '../public')));

// for system.js to work. Can be removed if bundling.
// app.use(express.static(path.resolve(__dirname, '../client')));
// app.use(express.static(path.resolve(__dirname, '../../node_modules')));

app.use(bodyParser.json({limit: '50mb'}));


app.use('/api', function(req, res, next) {
    passport.authenticate('jwt', { session: false }, function(err, user, info) {
        if (err) {
            res.status(403).json({ message: "Token could not be authenticated", fullError: err })
        }
        if (user) {
            req.user = user;
            return next();
        }
        return res.status(403).json({ message: "Token could not be authenticated", fullError: info });
    })(req, res, next);
});
app.use(helmet());

app.use('/api', new apiRoutes().routes);
app.use('/', new publicRoutes().routes);



var renderIndex = (req, res) => {
    res.sendFile(path.resolve(__dirname, '../dist/index.html'));
}

app.get('/', renderIndex);



if (env === 'developement') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            error: err,
            message: err.message
        });
    });
}


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error("Not Found");
    next(err);
});

// production error handler
// no stacktrace leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        error: {},
        message: err.message
    });
});